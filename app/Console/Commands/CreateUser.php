<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:user {name} {email} {phone} {sex} {invite_type} {password} {role}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create User with given data and role';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->validateCreate($this->argument());

        $data = [
            'name' => $this->argument('name'),
            'email' => $this->argument('email'),
            'phone' => $this->argument('phone'),
            'sex' => $this->argument('sex'),
            'invite_type' => $this->argument('invite_type'),
            'password' => bcrypt($this->argument('password'))
        ];

        $user = User::create($data);

        $roleInput = strtolower($this->argument('role'));
        $role = config('roles.models.role')::where('slug', '=', $roleInput)->first();

        $user->attachRole($role);
    }

    private function validateCreate($arguments)
    {
        $validator = Validator::make($arguments, [
            'name' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required|max:20|unique:users',
            'sex' => 'required|in:male,female',
            'invite_type' => 'required|in:send,accept',
            'role' => 'required|string'
        ]);

        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                echo $error . "\n";
            }
        }


    }

}
